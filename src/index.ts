import { readFileSync, writeFileSync } from 'fs';
import { findServices } from './manager';
import { ServiceConfig } from './service';
import { WebServer } from './web';

export interface Config {

    services: ServiceConfig[];

}

export const AppConfig: Config = JSON.parse(
    readFileSync("./config.json").toString()
);

export function flushConfig() {
    writeFileSync("./config.json", JSON.stringify(AppConfig, null, 2));
}

(async () => {
    await findServices(AppConfig.services);
    WebServer.start();
})();

