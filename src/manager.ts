import { Announcement, Service, ServiceConfig } from "./service";
import { readdir } from 'fs/promises';

export class Manager {

    private services = new Array<Service<any>>();

    registerService<T extends ServiceConfig>(service: Service<T>) {
        this.services.push(service);
    }

    async submit(announcement: Announcement): Promise<string[]> {
        return new Promise(async (resolve, reject) => {
            let postedServices: string[] = [];
            for (const service of this.services) {
                try {
                    await service.submit(announcement);
                    postedServices.push(service.config.id);
                } catch (ex) {
                    console.error(ex);
                }
            }
            resolve(postedServices);
        });
    }

}

export const ServiceManager = new Manager();

export async function findServices(configs: ServiceConfig[], path = `${__dirname}/services/`) {
    for (let file of await readdir(path)) {
        const imports = await import(path + file);
        if (!imports["ServiceId"]) 
                continue;
        for (const key of Object.keys(imports)) {
            if (key.endsWith("Service")) {
                const serviceNames: string[] = imports["ServiceId"];
                for (const serviceName of serviceNames) {
                    const config = configs.find(c => c.id === serviceName);
                    if (config) {
                        const service: Service<any> = new imports[key](config);
                        ServiceManager.registerService(service);
                        console.log(`Loaded service ${serviceName}`);
                    } else
                        console.warn(`Ignoring ${key}`);
                }
            }
        }
    }
}
