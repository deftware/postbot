import { Announcement, Service, ServiceConfig } from "../service";
import { post } from 'request';

export interface DiscordConfig extends ServiceConfig {

    /**
     * A list of webhooks to post to
     */
    webhooks: string[];

}

/**
 * Also supports guilded.gg
 */
export class DiscordService extends Service<DiscordConfig> {

    async submit(announcement: Announcement): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            let data = {
                "username": "Announcement",
                "avatar_url": "https://aristois.net/res/logo_big.png?v=1",
                "embeds": [{
                    "title": announcement.title,
                    "description": announcement.description,
                    "color": 15258703,
                    "author": {
                        "name": announcement.author.name,
                        "url": announcement.author.url,
                        "icon_url": "https://cdn.discordapp.com/embed/avatars/0.png"
                    }
                }]
            };
            if (announcement.author.icon)
                data.embeds[0].author.icon_url = announcement.author.icon;
            // Send webhook
            for (const url of this.config.webhooks) {
                post(url, {
                    json: data
                }, (error, response, body) => {
                    if (error)
                        return reject(error);
                    resolve();
                });
            }
        });
    }

}

export const ServiceId = [ "discord", "guilded" ];
