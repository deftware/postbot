import { Announcement, Service, ServiceConfig } from "../service";
import { post } from 'request';

export interface NodeBBConfig extends ServiceConfig {

    url: string;

    cid: string;

    auth: NodeBBAuth;

}

export interface NodeBBAuth {

    bearer: string;

    /**
     * User id
     */
    uid: string;

}

/**
 * Iterfaces with https://github.com/NodeBB/nodebb-plugin-write-api
 */
export class NodeBBService extends Service<NodeBBConfig> {

    async submit(announcement: Announcement): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            const data = {
                cid: this.config.cid,
                title: announcement.title,
                content: announcement.description,
                tags: [
                    "Announcement"
                ]
            };
            post(`${this.config.url}/topics/`, {
                json: data,
                headers: {
                    "Authorization": `Bearer ${this.config.auth.bearer}`
                }
            }, (error, response, body) => {
                if (error)
                    reject();
                resolve();
            });
        });
    }

}

export const ServiceId = [ "nodebb" ];
