import { Announcement, Service, ServiceConfig } from "../service";
import { randomBytes } from 'crypto';
import { post } from 'request';
import { WebServer } from "../web";
import { stringify } from 'querystring';
import { flushConfig } from "../index";

export interface RedditConfig extends ServiceConfig {

    subreddit: string;

    auth: RedditAuth;

}

export interface RedditAuth {

    client_id: string;

    bearer: string;

    refreshToken: string;

    authTime: number;

    oauthUrl: string;

    secret: string;

    host: string;

}

/**
 * Also supports guilded.gg
 */
export class RedditService extends Service<RedditConfig> {

    private auth: RedditAuthentication;

    constructor(config: RedditConfig) {
        super(config);
        this.auth = new RedditAuthentication(this);
    }

    async submit(announcement: Announcement): Promise<void> {
        return new Promise<void>(async (resolve, reject) => {
            const params = {
                sr: this.config.subreddit,
                text: announcement.description,
                title: announcement.title,
                kind: "self"
            }
            await this.auth.refresh();
            // Send API request
            post(`https://oauth.reddit.com/api/submit.json?${stringify(params)}`, {
                body: stringify({
                    "resubmit": "true",
                    "send_replies": "true",
                    "api_type": "json",
                }),
                headers: {
                    "Authorization": `bearer ${this.config.auth.bearer}`,
                    "Content-Type": "application/json",
                    "User-Agent": "nodejs:postbot:v1.0.0 (by /u/Moudoux)"
                }
            }, (error, response, body) => {
                if (error)
                    return reject(error);
                resolve();
            });
        });
    }

}

class RedditAuthentication {

    private service: RedditService;
    private authConfig: RedditAuth;
    private state = randomBytes(10).toString('hex');

    constructor(service: RedditService) {
        this.service = service;
        this.authConfig = this.service.config.auth;
        // Set up oauth callback
        WebServer.server.get(this.authConfig.oauthUrl, (req, res) => {
            this.onAuth(req.query.state as string, req.query.code as string);
            res.send("Ok");
        });
        // First time setup
        if (this.authConfig.bearer === "") {
            const authUrl = this.getAuthorizationUrl(this.state, "permanent");
            console.log(`Please visit ${authUrl} in your browser`);
        }
    }

    async onAuth(state: string, code: string) {
        const redirect_uri = this.authConfig.host + this.authConfig.oauthUrl;
        await this.getAccessToken({
            grant_type: "authorization_code",
            code,
            redirect_uri
        });
    }

    async refresh() {
        // Check if token expired
        if (this.authConfig.authTime < new Date().getTime()) {
            console.log("Refreshing reddit token");
            await this.getAccessToken({
                grant_type: "refresh_token",
                refresh_token: this.authConfig.refreshToken
            });
        }
    }

    async getAccessToken(params: any): Promise<void> {
        return new Promise((resolve, reject) => {
            const auth = Buffer.from(`${this.authConfig.client_id}:${this.authConfig.secret}`).toString('base64');
            post("https://www.reddit.com/api/v1/access_token", {
                body: stringify(params),
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded",
                    "Authorization": `Basic ${auth}`
                }
            }, (error, response, json) => {
                json = JSON.parse(json);
                if (json.error) {
                    console.error("Invalid oauth response");
                    reject();
                } else {
                    // Reddit gives us a token valid for 60 minutes
                    this.authConfig.authTime = new Date().getTime() + (json.expires_in * 1000);
                    this.authConfig.bearer = json.access_token;
                    this.authConfig.refreshToken = json.refresh_token;
                    flushConfig();
                    console.log("Successfully authenticated with reddit");
                    resolve();
                }
            });
        });
    }

    getAuthorizationUrl(state: string, duration: string): string {
        return `https://www.reddit.com/api/v1/authorize?client_id=${this.authConfig.client_id}&response_type=code&
        state=${state}&redirect_uri=${this.authConfig.host + this.authConfig.oauthUrl}&duration=${duration}&scope=${encodeURIComponent("identity edit history mysubreddits read submit flair")}`;
    }

}

export const ServiceId = [ "reddit" ];
