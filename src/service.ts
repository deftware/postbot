export interface Announcement {

    title: string;

    description: string;

    author: Author;

}

export interface Author {

    name: string;

    url: string;

    icon?: string;

}

export interface ServiceConfig {

    id: string;

}

export abstract class Service<T extends ServiceConfig> {

    public config: T;

    constructor(config: T) {
        this.config = config;
    }

    abstract submit(announcement: Announcement): Promise<void>;

}
