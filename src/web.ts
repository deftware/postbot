import express, { Express } from 'express';
import { ServiceManager } from './manager';
import { Announcement } from './service';

export class Server {

    public server: Express;

    constructor() {
        this.server = express();
        this.server.use(express.json());
        this.server.post("/submit", async (req, res) => {
            const announcement: Announcement = req.body.announcement;
            const data: string[] = await ServiceManager.submit(announcement);
            res.send(JSON.stringify({
                status: true,
                services: data
            }));
        });
        this.server.use(express.static("./html"));
    }

    start() {
        const port = process.env.WEB_PORT || "8080";
        const host = process.env.WEB_HOST || "0.0.0.0";
        this.server.listen(parseInt(port), host, () => {
            console.log(`Listening on port ${port}`);
        });
    }

}

export const WebServer = new Server();
